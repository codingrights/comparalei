# Enxergando mudanças entre versões de uma lei

Compara dois documentos, criando um documento onde é possível ver com um
código de cores quais trechos foram acrescentados/suprimidos/modificados. O script de conversão (``converter.py``) é otimizado para leis brasileiras.

## Instalação

Esse software foi feito em um sistema Debian, mas deve funcionar igualmente
para Ubuntu e provavelmente em outras distribuições (procure pelas
dependências).

Primeiro, instale as dependências:

- ``poppler-utils``: contém o comando ``pdftotext``
- ``dwdiff``: permite fazer diff's a nível de palavras (e não linhas) e com
  saída colorida.
- ``aha`` (Ansi HTML Adapter): converte a saída do ``dwdiff`` (cores ANSI p/ terminal) em um
  HTML 

```
# apt-get install poppler-utils dwdiff aha 
```

Em seguida, instale o módulo python ``docopt``, que usamos para criar uma
interface de linha de comando simples:

```
# pip install docopt
```

ou:


```
# easy_install docopt
```

Em seguida, clone esse repositório:

```
$ git clone https://gitlab.com/codingrights/comparalei/
```

## Uso

Comparando dois arquivos PDF:

```
./comparalei.py exemplos/APL_2015-1.pdf exemplos/APL_2015-2.pdf -o exemplos/comparacao_APLs.html
```

(se não a opção ``-o / --ouptut`` não for especificada, o HTML será exibido
na tela.

Comparando diretamente arquivos de texto:

```
./comparalei.py arquivo1.txt arquivo2.txt -o resultado.html
```

Esse comando é útil quando ainda se quer melhorar a conversão do PDF para
texto -- alguns problemas de formatação podem aparecer. Para fazer somente a
conversão dos arquivos, use o script ``converter.py``:

```
./converter.py exemplos/APL_2015-1.pdf
```

Por padrão, o resultado vai parar em um arquivo de mesmo nome, e extensão
``.txt``.

Para ver a ajuda completa dos comandos:

```
./comparalei.py --help
./converter.py --help
```
