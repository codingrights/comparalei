#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""
Cria um documento onde é possível ver com um código de cores quais
trechos foram acrescentados/suprimidos/modificados. O script de
conversão (``converter.py``) é otimizado para leis brasileiras. Such
software, much beta!

Usage:
    ./comparalei.py [--txt -o FILE] <arquivo1> <arquivo2>

Options:
    --txt                       Compara arquivos de texto diretamente, sem realizar a conversão de um PDF.
    -o FILE, --output FILE      Nome do arquivo HTML a ser criado. Se não for especificado, a saída é feita na tela.

"""

import sys
import codecs
from tempfile import mktemp
from subprocess import check_output, Popen, PIPE
from docopt import docopt

DWDIFF_FLAGS = ['-d', ',.', '-c']

if __name__ == '__main__':
    args = docopt(__doc__)
    arquivo1, arquivo2 = args['<arquivo1>'], args['<arquivo2>']

    if args['--txt']:
        txt1, txt2 = arquivo1, arquivo2
    else:
        txt1 = mktemp()
        txt2 = mktemp()

        check_output(['./converter.py', arquivo1, '--output', txt1])
        check_output(['./converter.py', arquivo2, '--output', txt2])

    dwdiff = Popen(['dwdiff'] + DWDIFF_FLAGS + [txt1, txt2], stdout=PIPE)
    html_output = check_output(['aha'], stdin=dwdiff.stdout).decode('utf-8')
    dwdiff.wait()

    output = args['--output']

    if output:
        with codecs.open(output, 'w', encoding='utf8') as outfile:
            outfile.write(html_output)
    else:
            print(html_output)
