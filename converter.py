#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""
Converte um PDF em texto. O resultado final provavelmente ainda
precisará de alguma edição manual para ficar perfeito, mas já é útil
para fazer comparações de mudanças.

Ele pode ser usado em qualquer tipo de PDF, mas é otimizado para
formatar leis brasileiras. A variável ``REGEXES`` lista uma série de
expressões regulares para esse fim -- comentá-las pode ser útil para não
zoar um texto que não seja uma lei.

Usage:
    ./converter.py [-f NUM -l NUM -o FILE] <arquivo-pdf>

Options:
    -o FILE, --output FILE       arquivo de saída (sem essa opção, vai pra tela)
    -f NUM, --first NUM          especifica primeira página a ser convertida
    -l NUM, --last NUM           especifica última página a ser convertida

"""

import re
import codecs

from subprocess import check_output
from docopt import docopt

#: optional flags to be sent to pdftext 
PDFTOTEXT_FLAGS = []

#: uma lista de tuples com as substituições a serem feitas:
#  o padrão, o que deve ir no lugar, e opcionalmente um valor pras
#  flags do módulo `re` (avançado) .
REGEXES = (
    # Junta capítulos e seções cujo título ficou dividido em duas linhas
    (ur'(CAPÍTULO [IXVMCL]+)\n+(.*)', ur'\1 – \2\n', re.IGNORECASE),
    (ur'(Seção [IXVMCL]+)\n+(.*)', ur'\1 – \2\n', re.IGNORECASE),

    (ur'^CAPITULO', ur'^CAPÍTULO'),

    # Garante que capítulos, seções, parágrafos únicos, artigos e incisos
    # têm linhas em branco antes e depois
    (ur'(CAPÍTULO.*)\n(\S)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(\S)\s*\n\s*(CAPÍTULO)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(Seção.*)\n(\S)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(\S)\s*\n\s*(Seção)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(Parágrafo único.*)\n(\S)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(\S)\n(Parágrafo único.*)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(Art[.\s].*)\n(\S)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(\S)\n(Art[.\s].*)', ur'\1\n\n\2', re.IGNORECASE),
    (ur'(§ .*)\n(\S)', ur'\1\n\n\2'),
    (ur'(\S)\n(§ .*)', ur'\1\n\n\2'),

    # Separa o primeiro inciso do artigo/parágrafo que o precede
    (ur'^(.*\S)\nI\. ', ur'\1\n\nI. '),

    # Tira esses caracteres de controle que surgem na conversão
    (ur'', ''),

    # Transforma as alíneas para torná-las listas válidas em Markdown
    (ur'^([a-z])\)', ur'    \1.  '),

    # Transforma os itens para torná-los listas válidas em Markdown
    (ur'^([0-9]+)\.', ur'        \1.  '),

    # Transforma os incisos para torná-los listas válidas em Markdown
    (ur'^([IXVMCL]+) +[-⁻–－﹣]', ur'\1. '),

)

def pdftotext(input_filename, first=None, last=None):
    if first:
        PDFTOTEXT_FLAGS.extend(['-f', first])
    if last:
        PDFTOTEXT_FLAGS.extend(['-l', last])

    cmdline = ['pdftotext'] + PDFTOTEXT_FLAGS + [input_filename, '-']
    stdout = check_output(cmdline)
    return stdout.decode('utf-8')

def fix_text(text):

    for regex in REGEXES:
        pattern, repl = regex[:2]

        flags = re.MULTILINE | re.UNICODE
        if len(regex) == 3:
            flags |= regex[2]

        try:
            text = re.sub(pattern, repl, text, flags=flags)
        except:
            print(pattern, repl)
            raise

    return text

if __name__ == '__main__':
    args = docopt(__doc__)
    text = pdftotext(args['<arquivo-pdf>'], args['--first'], args['--last'])

    text = fix_text(text)
    if args['--output']:
        output = args['--output']
    else:
        output = args['<arquivo-pdf>'].replace('.pdf', '.txt')
        # caso o arquivo esteja sem extensão pdf
        if output == args['<arquivo-pdf>']:
            output += '.txt'

    with codecs.open(output, 'w', encoding='utf8') as outfile:
        outfile.write(text)
